$(document).ready(function(){

	if($('.reg-form .inner-wrap').length > 0){
		$(".reg-form").validate({
	        rules: {
	            'ctl00$contentBody$boxRegion': {
			      required: true
			    },
			    'ctl00$contentBody$boxPoint': {
			      required: true
			    },
			    'ctl00$contentBody$phone': {
			      required: true
			    },
			    'ctl00$contentBody$last_name': {
			      required: true
			    },
			    'ctl00$contentBody$first_name': {
			      required: true
			    },
			    'ctl00$contentBody$patronymic': {
			      required: true
			    },
			    'ctl00$contentBody$boxSex': {
			      required: true
			    },
			    'ctl00$contentBody$place': {
			      required: true
			    },
			    'ctl00$contentBody$birthday': {
			      required: true
			    },
			    'ctl00$contentBody$boxCityzenship': {
			      required: true
			    },
			    'ctl00$contentBody$boxDocType': {
			      required: true
			    },
			    'ctl00$contentBody$passport_series': {
			      required: true
			    },
			    'ctl00$contentBody$passport_number': {
			      required: true
			    },
			    'ctl00$contentBody$passport_ufms': {
			      required: true
			    },
			    'ctl00$contentBody$passport_date': {
			      required: true
			    },
			    'ctl00$contentBody$passport_code': {
			      required: true
			    },
			    'ctl00$contentBody$passport_address': {
			      required: true
			    },
			    'ctl00$contentBody$file': {
			      required: true
			    }
	        },
	        errorElement : 'span',
	        errorPlacement: function(error, element) {
	          var placement = $(element).data('error');
	          var label = $(element).siblings('label');
	          if (placement) {
	            $(label).append(error)
	          } else {
	            error.insertAfter(label);
	          }
	        }
	 	});
	}

	// if($('.reg-form .lk-wrap').length > 0){
	//  	$(".reg-form").validate({
	//         rules: {
	// 		    'ctl00$contentBody$passport_series': {
	// 		      required: true
	// 		    },
	// 		    'ctl00$contentBody$passport_number': {
	// 		      required: true
	// 		    },
	// 		    'ctl00$contentBody$passport_ufms': {
	// 		      required: true
	// 		    },
	// 		    'ctl00$contentBody$passport_date': {
	// 		      required: true
	// 		    },
	// 		    'ctl00$contentBody$passport_code': {
	// 		      required: true
	// 		    },
	// 		    'ctl00$contentBody$passport_address': {
	// 		      required: true
	// 		    }
	//         },
	//         errorElement : 'span',
	//         errorPlacement: function(error, element) {
	//           var placement = $(element).data('error');
	//           var label = $(element).siblings('label');
	//           if (placement) {
	//             $(label).append(error)
	//           } else {
	//             error.insertAfter(label);
	//           }
	//         }
	//  	});

	//  	$('#save-passport').click(function(){
	// 		$('.reg-form').valid();
	// 	});
	// }

	if($('.reg-form .lk-wrap').length > 0){
	 	$(".reg-form").validate({
	        rules: {
			    'passport-series': {
			      required: true
			    },
			    'passport-number': {
			      required: true
			    },
			    'passport-ufms': {
			      required: true
			    },
			    'passport-date': {
			      required: true
			    },
			    'passport-code': {
			      required: true
			    },
			    'passport-address': {
			      required: true
			    }
	        },
	        errorElement : 'span',
	        errorPlacement: function(error, element) {
	          var placement = $(element).data('error');
	          var label = $(element).siblings('label');
	          if (placement) {
	            $(label).append(error)
	          } else {
	            error.insertAfter(label);
	          }
	        }
	 	});

	 	$('#save-passport').click(function(){
			$('.reg-form').valid();
		});
	}

	$('.modal').modal();
	$('.passport-date').datepicker({
	    maxDate: new Date()
	});
	$('.birthday').datepicker({
	    maxDate: "-18y"
	});
	$( ".datepicker-here" ).datepicker( $.datepicker.regional[ "ru" ] );
  	$('.phone').inputmask({
		'mask': '+7(999)999-99-99',
		"clearIncomplete": true
  	});
  	$('.passport-series').inputmask({
		'mask': '9999',
		"clearIncomplete": true
  	});
  	$('.passport-number').inputmask({
		'mask': '999999',
		"clearIncomplete": true
  	});
  	$('.passport-code').inputmask({
		'mask': '999-999',
		"clearIncomplete": true
  	});
  	$('.datepicker-here').inputmask({
		'mask': '99.99.9999',
		"clearIncomplete": true
  	});

  	$('.lk-wrap .passport-date').inputmask({
		'mask': '99.99.9999',
		"clearIncomplete": true
  	});

	// если данные успешно сохранены
	$('#modal-success').modal('open');

	$('.main-btn input[type="submit"]').click(function(){
		var region = $('input[name="ctl00$contentBody$boxRegion"]');
		if((region.val() == 'Выберите регион')) {
			html='';
			html+='<span class="error">Это поле необходимо заполнить.</span>';
			region.parent().removeClass('valid');
			region.parent().addClass('invalid');
			region.parent().find('.error').remove();
			region.parent().append(html);
	    } else {
			region.parent().removeClass('invalid');
			region.parent().addClass('valid');
			$(this).submit();
	    }
	});
	$('.main-btn input[type="submit"]').click(function(){
		var boxpoint = $('input[name="ctl00$contentBody$boxPoint"]');
		if((boxpoint.val() == 'Выберите торговую точку')) {
			html='';
			html+='<span class="error">Это поле необходимо заполнить.</span>';
			boxpoint.parent().removeClass('valid');
			boxpoint.parent().addClass('invalid');
			boxpoint.parent().find('.error').remove();
			boxpoint.parent().append(html);
	    } else {
	    	boxpoint.parent().removeClass('invalid');
			boxpoint.parent().addClass('valid');
			$(this).submit();
	    }
	});

  	$('.get-pass').click(function(){
  		if($('#phone').val() != '') {
  			$('.pswd-row').show();
  			$(this).text('Подтвердить');
  			// $(this).removeClass('get-pass').addClass('submit-pass disabled');
  			$(this).removeClass('get-pass').addClass('submit-pass');
  			let dt = new Date();
			let time = dt.getFullYear() + '/' + (dt.getMonth()+1) + '/' + dt.getDate() + ' ' + dt.getHours() + ":" + (dt.getMinutes()+2) + ":" + dt.getSeconds();
			$('.clock').parent().show();
			$('.clock').countdown(time)
			.on('update.countdown', function(event) {
			  	$(this).html(event.strftime('%M:%S'));
			})
			.on('finish.countdown', function(event) {
				$(this).parent().hide();
				// $('.submit-pass').removeClass('disabled');
				$(this).parent().siblings('.resend-link').show();
			});
			// повторная отправка смс
		    $('.resend-link').click(function(e){
				e.preventDefault();
				$(this).hide();
				// $('submit-pass').addClass('disabled');
				let dt = new Date();
				let time = dt.getFullYear() + '/' + (dt.getMonth()+1) + '/' + dt.getDate() + ' ' + dt.getHours() + ":" + (dt.getMinutes()+2) + ":" + dt.getSeconds();
				$('.clock').parent().show();
				$('.clock').countdown(time)
				.on('update.countdown', function(event) {
				  	$(this).html(event.strftime('%M:%S'));
				})
				.on('finish.countdown', function(event) {
					$(this).parent().hide();
					// $('.submit-pass').removeClass('disabled');
					$(this).parent().siblings('.resend-link').show();
				});
			});
  			
  			$('.submit-pass').click(function(e){
		  		e.preventDefault();
		  		if(($('#phone').val() != '') && ($('#pswd-sms').val() != '')) {
		  			//ajax
		  			console.log('ajax forgot pswd');
		  		}
		  	});
  		}
  	});

  	$('#edit-passport').click(function(e){
		e.preventDefault();
  		$(this).hide();
		$('#passport-data').find('input, textarea').removeAttr('disabled');
		$(this).parent().find('.edit-link').show();
	});

	$('#save-passport').click(function(e){
		e.preventDefault();
		//ajax
		console.log('ajax save passport changes');
	});

	$('.edit-phone').click(function(e){
		e.preventDefault();
		$(this).parent().parent().find('.phone, .main-select').removeAttr('disabled');
		$(this).hide();
		$(this).parent().find('.save-phone, .delete-phone').show();
	});

	$('#add-phone').click(function(){
		html = '';
		html += `<div class="row new mb0">
					<div class="input-field col s12 xl2">
						<label for="phone_1" class="main-label active">Номер телефона</label>
						<input id="phone_1" type="text" class="main-input validate phone" placeholder="+7(___)___-__-__" value="+7(999)999-99-99">
					</div>
					<div class="input-field col s12 xl2">
						<label class="main-label active">Регион</label>
				  		<select class="main-select browser-default">
				    		<option value="" disabled>Регион</option>
				    		<option value="Москва" selected>Москва</option>
				    		<option value="2">Option 2</option>
				    		<option value="3">Option 3</option>
				  		</select>
					</div>
					<div class="input-field col s12 xl2">
						<label class="main-label active">Торговая точка</label>
				  		<select class="main-select browser-default">
				    		<option value="" disabled selected>Торговая точка</option>
				    		<option value="Торговая точка 1">Торговая точка 1</option>
				    		<option value="2">Option 2</option>
				    		<option value="3">Option 3</option>
				  		</select>
					</div>
					<div class="input-field col s12 xl2">
						<label class="main-label active">Действие</label>
			          	<a href="#" class="main-link main-color phone-link edit-phone save-phone-new">Сохранить</a>
			        </div>
		      	</div>`;
		$('#phone-data').append(html);

		$('.phone').inputmask({
			'mask': '+7(999)999-99-99',
			"clearIncomplete": true
	  	});

		$('.edit-phone').click(function(e){
			e.preventDefault();
			$(this).parent().parent().find('.phone, .main-select').removeAttr('disabled');
			$(this).hide();
			$(this).parent().find('.save-phone, .delete-phone').show();
		});
	});
});